package main

import "fmt"

func Chunks(str string, size int) (chunks []string) {
	runes := []rune(str)
	runesLen := len(runes)
	chunksLen := runesLen / size
	if (runesLen % size) > 0 {
		chunksLen++
	}
	chunks = make([]string, chunksLen)
	bgn := 0
	for i := 0; i < chunksLen; i++ {
		end := bgn + size
		if runesLen < end {
			chunks[i] = string(runes[bgn:])
		} else {
			chunks[i] = string(runes[bgn:end])
		}
		bgn = end
	}
	return
}
func main() {
	fmt.Println("Hello,_世界")
	chunks := Chunks("Hello,_世界Hello,_世界Hello,_世界", 4)
	fmt.Println(chunks)
}
